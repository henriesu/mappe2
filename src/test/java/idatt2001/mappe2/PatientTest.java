package idatt2001.mappe2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.File;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PatientTest {
    Controller controller;
    PatientRegister patientRegister;

    @BeforeEach
    void setUpController(){
        this.controller = new Controller();
        this.patientRegister = new PatientRegister();
    }

    @Test
    void testCheckIfCsvFile(){
        File file = new File("/Mappe2/Mappe2.iml");
        assertThrows(IllegalArgumentException.class, () -> controller.getContentType(file));
    }

    @Test
    void checkIfPatientCanBeAddedInController(){
        Patient patient = new Patient( null, "Tusvik",null);
        assertThrows(IllegalArgumentException.class, () -> controller.addPatientToPatientRegister(patient));
    }

    @Test
    void checkIfPatientCanBeAddedInRegister(){
        Patient patient = new Patient("Hei", "Sann", "02020288899");
        assertThrows(IllegalArgumentException.class, () -> patientRegister.addPatient(patient));
    }
}
