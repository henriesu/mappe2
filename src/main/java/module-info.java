module idatt2001.mappe2 {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;

    opens idatt2001.mappe2 to home.fxml;
    exports idatt2001.mappe2;
}