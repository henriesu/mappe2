package idatt2001.mappe2;

/**
 * Patient class for initializing objects of Patient
 *
 * @author Henriette Brekke Sunde
 */
public class Patient {
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Default constructor
     */
    public Patient(){}

    /**
     * Constructor for creating a patient with all the possible variables
     * @param firstName,lastName,generalPractitioner,socialSecurityNumber,diagnosis
     */
    public Patient(String firstName, String lastName, String generalPractitioner, String socialSecurityNumber, String diagnosis){
        if(firstName == null || lastName == null || socialSecurityNumber == null){
            throw new IllegalArgumentException("Field cannot be empty!");
        }
        this.firstName = firstName;
        this.lastName = lastName;
        if(socialSecurityNumber.matches("[0-9]+")){
            this.socialSecurityNumber = socialSecurityNumber;
        }else{
            throw new IllegalArgumentException("Socialsecuritynumber has to be 11 digits and written in numbers!");
        }
        //this.socialSecurityNumber = socialSecurityNumber;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Constructor for creating a patient with all the necessary possible variables
     * @param firstName,lastName,socialSecurityNumber
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber){
        if(firstName == null || lastName == null || socialSecurityNumber == null){
            throw new IllegalArgumentException("Field cannot be empty!");
        }
        this.firstName = firstName;
        this.lastName = lastName;
        if(socialSecurityNumber.matches("[0-9]+")){
            this.socialSecurityNumber = socialSecurityNumber;
        }else{
            throw new IllegalArgumentException("Socialsecuritynumber has to be 11 digits and written in numbers!");
        }
        //this.socialSecurityNumber = socialSecurityNumber;
    }

    //Getters
    public String getFirstName() { return firstName; }
    public String getLastName() { return lastName; }
    public String getSocialSecurityNumber() { return socialSecurityNumber; }
    public String getDiagnosis() { return diagnosis; }
    public String getGeneralPractitioner() { return generalPractitioner; }

    //Setters
    public void setFirstName(String firstName) { this.firstName = firstName; }
    public void setLastName(String lastName) { this.lastName = lastName; }
    public void setSocialSecurityNumber(String socialSecurityNumber) { this.socialSecurityNumber = socialSecurityNumber; }
    public void setDiagnosis(String diagnosis) { this.diagnosis = diagnosis; }
    public void setGeneralPractitioner(String generalPractitioner) { this.generalPractitioner = generalPractitioner; }

    /**
     * Method for creating a string of a patient-object
     * @return string of patient
     */
    public String getAsString() {
        return String.format("%s %s %s", firstName, lastName, socialSecurityNumber);
    }
}
