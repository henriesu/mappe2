package idatt2001.mappe2.factorydesign;

class GetGUIFactory {

    public GUI getGUI(String GUIType) {
        if(GUIType == null){
            return null;
        }else if(GUIType.equalsIgnoreCase("BUTTON")){
            return new Buttons();
        }else if(GUIType.equalsIgnoreCase("LABEL")){
            return new Labels();
        }else if(GUIType.equalsIgnoreCase("MENU")){
            return new Menus();
        }else if(GUIType.equalsIgnoreCase("MENUBAR")){
            return new MenuBars();
        }else if(GUIType.equalsIgnoreCase("MENUITEM")){
            return new MenuItems();
        }else if(GUIType.equalsIgnoreCase("TABLECOLUMN")){
            return new TableColumns();
        }else if(GUIType.equalsIgnoreCase("TABLEVIEW")){
            return new TableViews();
        }else if(GUIType.equalsIgnoreCase("TEXTFIELD")){
            return new TextFields();
        }
        return null;
    }
}
