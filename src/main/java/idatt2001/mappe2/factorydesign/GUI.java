package idatt2001.mappe2.factorydesign;

public abstract class GUI {

    abstract void createObject();
}
