package idatt2001.mappe2.factorydesign;

import java.io.BufferedReader;
import java.io.InputStreamReader;

class GenerateGUIComponent {
    public static void main(String[] args) throws Exception{
        GetGUIFactory guiFactory = new GetGUIFactory();

        System.out.println("GUI components: Button, Label, Menu, MenuBar, MenuItem, TableColumn, TableView, TextField");
        System.out.println("\n\nEnter the name of the component you want to create: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String guiName = br.readLine();
        System.out.print("\nEnter the number of component-units: ");
        int units = Integer.parseInt(br.readLine());

        //GUI gui = guiFactory.getGUI(guiName);

        System.out.print("\n You have created "+units+ " of "+guiName+".");
    }
}
