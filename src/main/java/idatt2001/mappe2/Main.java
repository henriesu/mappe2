package idatt2001.mappe2;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.util.logging.Logger;
import java.io.IOException;

/**
 * Main class of a Patient Register Application
 * Assignment: Mappe - Del 2 // Subject: IDATT2001
 *
 * @author Henriette Brekke Sunde
 */
public class Main extends Application{
    private static Scene scene;
    private static Stage stage;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    /**
     * Start-method which makes the application run
     *
     * @param primaryStage
     * @throws IOException
     */
    public void start(Stage primaryStage) throws IOException {
        this.stage = primaryStage;
        try {
            scene = new Scene(loadFXML("home"));

            primaryStage.setScene(scene);
            primaryStage.setTitle("Patient register");
            primaryStage.show();

        } catch (IOException e) {
            logger.severe("ERROR: An IOException occured. Caused by " + e.getCause());
            throw e;
        }
    }

    public static Stage getStage(){
        return stage;
    }

    /**
     * setRoot-method is able to automatically set scene to a .fxml-file by simply typing in name of file
     *
     * @param fxml
     * @throws IOException
     */
    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    /**
     * loadFXML is a simple load method. Makes it possible to switch scenes
     * @param fxml
     * @throws IOException
     */
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args){
        launch(args);
    }
}
