package idatt2001.mappe2;

import java.util.ArrayList;
import java.util.List;

/**
 * PatientRegister class for initializing object of PatientRegister which can hold multiple objects of Patient
 *
 * @author Henriette Brekke Sunde
 */
public class PatientRegister {
    private ArrayList<Patient> patients = new ArrayList<>();

    /**
     * Constructor
     * Adds patients in standard PatientRegister with fillWithPatients()-method
     */
    public PatientRegister() {
        this.fillWithPatients();
    }

    /**
     * Adds "dummy"-patients to PatientRegister
     */
    public void fillWithPatients(){
        this.patients.add(new Patient("Hans", "Mortensen", "Edel Gran", "09209093421", "Diabetes"));
        this.patients.add(new Patient("Mona", "Listen", "Mona Lisa","90435439532","Iron defiency"));
    }

    //Getter
    public List<Patient> getPatients() {
        return this.patients;
    }

    /**
     * Method for adding an object of patient to PatientRegister
     * Prints out message if patient already exists in the register
     * @param patient
     */
    public void addPatient(Patient patient) {
        if(!patients.contains(patient)){
            this.patients.add(patient);
        }else{
            System.out.println("Not able to add patient");
        }

    }
}
