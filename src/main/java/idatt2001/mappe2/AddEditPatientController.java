package idatt2001.mappe2;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import java.io.IOException;

/**
 * Second controller for this application
 * Used to handle events in addPatient.fxml and editPatient.fxml
 *
 * @author Henriette Brekke Sunde
 */
public class AddEditPatientController {
    @FXML public Button okButton;
    @FXML public Button cancelButton;
    @FXML public TextField firstNameInput;
    @FXML public TextField lastNameInput;
    @FXML public TextField generalInput;
    @FXML public TextField socSecNumInput;
    @FXML public TextField diagnosisInput;

    private Patient patientSelected;

    /**
     * Switches scene back to home.fxml
     * Creates new patient which is sent back to main controller using method
     *      addPatientToPatientRegister(new patient)
     *
     * @throws IOException
     */
    @FXML
    public void addPatientOK() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("home.fxml"));
        Parent root = loader.load();

        Controller homeController = loader.getController();
        try{
            Patient patient = new Patient(firstNameInput.getText(), lastNameInput.getText(), generalInput.getText(), socSecNumInput.getText(), diagnosisInput.getText());
            homeController.addPatientToPatientRegister(patient);
        }catch(Exception e){
            Alert mustBeNumber = new Alert(Alert.AlertType.INFORMATION);
            mustBeNumber.setTitle("Wrong Input");
            mustBeNumber.setHeaderText("Woopsies!");
            mustBeNumber.setContentText("Socialsecuritynumber HAS to be a number (int)! Please try again.");
            mustBeNumber.showAndWait();
        }

        Main.getStage().setScene(new Scene(root));
        Main.getStage().setTitle("Patient register");
        Main.getStage().show();
    }

    /**
     * Method for setting textFields in editPatient.fxml to the same values as the selected patient has
     *
     * @param patient
     */
    @FXML
    public void initData(Patient patient){
        patientSelected = patient;

        firstNameInput.setText(patientSelected.getFirstName());
        lastNameInput.setText(patientSelected.getLastName());
        generalInput.setText(patientSelected.getGeneralPractitioner());
        socSecNumInput.setText(patientSelected.getSocialSecurityNumber());
        diagnosisInput.setText(patientSelected.getDiagnosis());
    }

    /**
     * Switches scene back to home.fxml
     * Sends new (edited) information about selected patient back to main controller using method
     *      editPatient(firstName, lastName, generalPractitioner, socialSecurityNumber, diagnosis)
     * @throws IOException
     */
    @FXML
    public void editPatientOK() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("home.fxml"));
        Parent root = loader.load();

        Controller homeController = loader.getController();
        homeController.editPatient(firstNameInput.getText(), lastNameInput.getText(), generalInput.getText(), socSecNumInput.getText(), diagnosisInput.getText());

        Main.getStage().setScene(new Scene(root));
        Main.getStage().setTitle("Patient register");
        Main.getStage().show();
    }

    /**
     * If user choose to cancel the act of adding or editing the program will switch scenes back to home.fxml
     * Nothing is added or changed
     * @throws IOException
     */
    public void cancel() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("home.fxml"));
        Parent root = loader.load();
        Main.getStage().setScene(new Scene(root));
        Main.getStage().setTitle("Patient register");
        Main.getStage().show();
    }
}