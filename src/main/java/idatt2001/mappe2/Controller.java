package idatt2001.mappe2;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;

import java.io.*;
import java.nio.file.Files;
import java.util.Optional;

/**
 * Main controller for this project
 *
 * @author Henriette Brekke Sunde
 */
public class Controller extends BorderPane {
    @FXML public TableView<Patient> patientView;
    @FXML public TableColumn<Patient, String> firstNameColumn;
    @FXML public TableColumn<Patient, String> lastNameColumn;
    @FXML public TableColumn<Patient, String> generalPractitionerColumn;
    @FXML public TableColumn<Patient, String> socialColumn;
    @FXML public TableColumn<Patient, String> diagnoseColumn;
    @FXML public Button addPatientButton;
    @FXML public Label status;

    private PatientRegister patientRegister;
    private ObservableList<Patient> patientObservableList;


    /**
     * Initalize is able to load the scene and it's children
     * We can start using the application only after it has been initialized
     */
    @FXML
    public void initialize() {
        this.patientRegister = new PatientRegister();

        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        socialColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        diagnoseColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));

        this.patientObservableList = FXCollections.observableArrayList(this.patientRegister.getPatients());

        this.patientView.setItems(this.patientObservableList);

        this.patientView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        status.setText("Status: OK");
    }

    /**
     * Set on action by Button addPatientButton and MenuItem add_menu
     * Sends us to a new scene for addPatient.fxml which is handled by AddEditPatientController
     *
     * @throws IOException
     */
    @FXML
    public void addNewPatient() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("addPatient.fxml"));
        Parent root = loader.load();

        Main.getStage().setScene(new Scene(root));
        Main.getStage().setTitle("Patient details - Add Patient");
        Main.getStage().show();
    }

    /**
     * Set on action by Button editPatientButton and MenuItem edit_menu
     * Sends us to a new scene for editPatient.fxml which is handled by AddEditPatientController
     *
     * @throws IOException
     */
    public void editSelectedPatient() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("editPatient.fxml"));
        Parent root = loader.load();

        AddEditPatientController editController = loader.getController();
        editController.initData(patientView.getSelectionModel().getSelectedItem());

        Main.getStage().setScene(new Scene(root));
        Main.getStage().setTitle("Patient details - Edit Patient");
        Main.getStage().show();
    }

    /**
     * @return index of selected patient
     */
    public int patientSelected() {
        int patient = patientView.getSelectionModel().getFocusedIndex();
        return patient;
    }

    /**
     * Implements two new ObservableList<Patient>
     *     one for all patients in tableView and one for all selected patients in tableView
     *     Removes all the selected patients from the observablelist for all the patients
     */
    public void deleteSelected(){
        ObservableList<Patient> allPatients, selectedPatients;
        allPatients = patientView.getItems();
        selectedPatients = patientView.getSelectionModel().getSelectedItems();

        for(Patient patient : selectedPatients){
            allPatients.remove(patient);
        }
    }

    /**
     * Creates pop-up dialog of AlertType: CONFIRMATION
     * Asks user if they want to delete selected patients
     * If user press OK button, it uses the method for deleting patients:
     *      deleteSelected()
     * If user press cancel it simply closes the pop-up dialog
     */
    public void removeSelectedPatient() {
        Alert delete = new Alert(Alert.AlertType.CONFIRMATION);
        delete.setTitle("Delete Confirmation");
        delete.setHeaderText("Delete Confirmation");
        delete.setContentText("Are you sure you want to delete this patient?");

        Optional<ButtonType> result = delete.showAndWait();
        if(result.get() == ButtonType.OK){
            deleteSelected();
        }else{
            delete.close();
        }
    }

    /**
     * Method for importing CSV-file from the users local computer.
     * Possible to do so with FileChooser and BufferedReader
     * Checks if File is of type .csv
     *      if not a pop-up dialog will be shown, telling the user that the file type is invalid for importing
     * Adds elements in csv file to observableList, which will update the tableView in the initialize-method
     * Finally block, as closing the reader is the last thing to be done
     */
    public void importCSV() {
        FileChooser fc = new FileChooser();
        File selectedFile = fc.showOpenDialog(null);
        BufferedReader reader = null;
        String line = "";
        int numberOfLines = 0;
        try{
            reader = new BufferedReader(new FileReader(selectedFile));
            getContentType(selectedFile);
            while((line = reader.readLine()) != null){
                numberOfLines++;
                while((line = reader.readLine()) != null) { //Run the while-loop a second time to remove the headlines
                    String[] row = line.split(";");

                    //Method for adding a zero in front of patient's socialsecuritynumber that only has a length of 10
                    for(int i = 0; i < numberOfLines; i++) {
                        if (row[3].length() == 10) {
                            String str = String.format("%11s", row[3]);
                            str = str.replace(' ', '0');
                            patientObservableList.add(new Patient(row[0], row[1], row[2], str, null));
                        }
                        //If length is correct we just add the number as it is
                        else if (row[3].length() == 11) {
                            patientObservableList.add(new Patient(row[0], row[1], row[2], row[3], null));
                        }
                    }
                    status.setText("Status: Import successful!");
                }
            }
        }catch(Exception e){
            Alert error = new Alert(Alert.AlertType.INFORMATION);
            error.setTitle("Error - Wrong file");
            error.setHeaderText("The chosen file type is not valid.");
            error.setContentText("Please chose another file by clicking on Select File or cancel the operation.");
            error.showAndWait();

            status.setText("Status: Import unsuccessful...");
        }finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method for exporting CSV-file to the users local computer.
     * Possible to do so with FileChooser, FileChooser.ExtensionFilter and FileWriter
     * Adds header to the .csv-file
     * Adds the patients in the observableList to filewriter which creates the new file
     * If the user already has a file with the same name
     *      a pop-up dialog will be shown, asking the user if the file should be overwritten or if they want to cancel
     */
    public void exportCSV() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        fileChooser.getExtensionFilters().add(extensionFilter);
        File file = fileChooser.showSaveDialog(null);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            fileWriter.write("firstName;lastName;generalPractitioner;socialSecurityNumber;diagnosis;\n");
            for(Patient patient : patientObservableList){
                fileWriter.write(patient.getFirstName()+";"+patient.getLastName()+";"+patient.getGeneralPractitioner()
                +";"+patient.getSocialSecurityNumber()+";"+patient.getDiagnosis()+";\n");
            }
            status.setText("Status: Export successful!");
            fileWriter.close();
        } catch (IOException e) {
            Alert overwrite = new Alert(Alert.AlertType.WARNING);
            overwrite.setTitle("Warning");
            overwrite.setHeaderText("A file with the same name exists.");
            overwrite.setContentText("Do you want to overwrite it?");
            overwrite.showAndWait();

            status.setText("Status: Export unsuccessful...");
        }
    }

    /**
     * Method for checking if a file to be imported is of the type text/csv
     * @param file
     * @return String
     * @throws IOException
     */
    public String getContentType(File file) throws IOException{
        String checkedType = Files.probeContentType(file.toPath());

        if(checkedType.equalsIgnoreCase("text/csv")){
            return checkedType;
        }
        return null;
    }

    /**
     * Method for exiting the program
     */
    @FXML
    public void exit() {
        Platform.exit();
    }

    /**
     * Creates pop-up dialog which tells the user about the application
     * Pop-up dialog is of AlertType: INFORMATION
     */
    public void helpAbout() {
        Alert help = new Alert(Alert.AlertType.INFORMATION);
        help.setTitle("Information Dialog - About");
        help.setHeaderText("Patients Register\n" + "v0.1-SNAPSHOT");
        help.setContentText("An application made for showing this student's qualifications in javafx gui, " +
                "factory design pattern and GitLab.\n" +
                "Qualifications shown accordingly to 'Mappe - Del 2' assignment.\n \n" + "(C) Henriette Brekke Sunde\n" + "\n" + "22-04-21");
        help.showAndWait();
    }

    /**
     * Adds patient to both patientRegister and observableList for patients
     * @param patient
     */
    @FXML
    public void addPatientToPatientRegister(Patient patient){
        this.patientRegister.addPatient(patient);
        this.patientObservableList.setAll(this.patientRegister.getPatients());
    }

    /**
     * Edits a selected patient. Information in parameters is received from AddEditPatientController
     * @param firstName,lastName,generalPractitioner,socialSecurityNumber,diagnosis
     */
    public void editPatient(String firstName, String lastName, String generalPractitioner, String socialSecurityNumber, String diagnosis) {
        Patient editPatient = patientObservableList.get(patientSelected());

        editPatient.setFirstName(firstName);
        editPatient.setLastName(lastName);
        editPatient.setGeneralPractitioner(generalPractitioner);
        editPatient.setSocialSecurityNumber(socialSecurityNumber);
        editPatient.setDiagnosis(diagnosis);
    }
}
